import asyncio
from bleak import BleakScanner
from bleak import BleakClient

#address = "CF:F7:C1:18:80:71" #Adresse MAC de l'appareil auquel on veut se connecter
nom = "OBU-5160" #Nom de l'appareil auquel on veut se connecter
fichier = open("data.txt", "wb") #Fichier où on écrit les données

def callback(sender, data):
    print(f"{sender}: {data}")


def callbackREAD(sender, data):
    print(f"{sender}: {data}")
    fichier.write(data)


async def main():
    print("Programme lancé")
    
    print("Recherche du boitier...")
    device = await BleakScanner.find_device_by_name(nom)
    print("Boitier trouvé !")
    
    print("Connexion au boitier...")
    async with BleakClient(device) as client:
        print("Connecté au boitier")
        
        services = client.services #Récupération de la liste des services du boitier
        
        char_lookup = {} #Dictionnaire des caracteristiques des services
        
        print("Affichage des services :")
        for s in services:
            #print(s)
            for c in s.characteristics:
                #print(c)
                char_lookup[c.description] = c.uuid #Remplissage du dictionnaire
        
        print("Récupération des caractéristiques effectuée")
        
        await client.start_notify(char_lookup["ACK"], callback)
        await client.start_notify(char_lookup["LOG"], callback)
        await client.start_notify(char_lookup["LIST"], callback)
        await client.start_notify(char_lookup["READ"], callbackREAD)
        print("Abonné aux notifications")
        
        N = await client.read_gatt_char(char_lookup["NUM"]) #Récupération du nombre de fichier
        #print(N)
        
        await client.write_gatt_char(char_lookup["LIST"], N)
        
        FileRequest = b'T5160\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x11'
        await client.write_gatt_char(char_lookup["READ"], FileRequest) 
        
        await asyncio.sleep(5)

asyncio.run(main())