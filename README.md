THOUMIN Maxime

On utilise les bibliothèques asyncio et bleak version 0.21.1

Pour lancer le programme, il suffit d'éxécuter "sec-objets-connexion.py" sans paramètres.

Pour modifier le nom du boitier avec lequel on souhaite se connecter, il faut modifier la ligne 6 du programme.

Pour modifier le nom du fichier où seront écrit les données, il faut modifier la ligne 7 du programme.

Il faut entrer manuellement le numéro du boitier à la ligne 53.